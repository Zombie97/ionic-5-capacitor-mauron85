import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataService {
/**
   * user's info object
   * @param {BehaviorSubject}
   */
  private userInfo = new BehaviorSubject({});

  /**
   * Current user info
   */
  currentUserInfo = this.userInfo.asObservable();
  constructor() { }
  updateUserData(addUserInfo) {
    const newUserInfo = Object.assign(this.userInfo, addUserInfo);
    this.userInfo.next(newUserInfo);
    return newUserInfo;
  }
}
