import { TestBed } from '@angular/core/testing';

import { BackgroundTrackService } from './background-track.service';

describe('BackgroundTrackService', () => {
  let service: BackgroundTrackService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BackgroundTrackService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
