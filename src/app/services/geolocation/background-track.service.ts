import { Injectable } from '@angular/core';
import { BackgroundGeolocation, BackgroundGeolocationConfig, BackgroundGeolocationEvents, BackgroundGeolocationResponse } from '@ionic-native/background-geolocation/ngx';
import { Diagnostic } from '@ionic-native/diagnostic/ngx';
import { AlertController } from '@ionic/angular';
import { DataService } from '../data/data.service';
@Injectable({
  providedIn: 'root'
})
export class BackgroundTrackService {

  constructor(private backgroundGeolocation: BackgroundGeolocation, private diagnostic: Diagnostic, private alertController: AlertController, 
    private dataService: DataService) { }
  initGeolocation(){
    console.log('[INIT] Geolocation')
    const config: BackgroundGeolocationConfig = {
      desiredAccuracy: 10,
      stationaryRadius: 20,
      distanceFilter: 30,
      debug: true, //  enable this hear sounds for background-geolocation life-cycle.
      stopOnTerminate: false, // enable this to clear background location settings when the app terminates
  };
  
  this.backgroundGeolocation.configure(config)
  .then(() => {
  
  this.backgroundGeolocation.on(BackgroundGeolocationEvents.location).subscribe(async (location: BackgroundGeolocationResponse) => {
  console.log(location);
  await this.dataService.updateUserData(location);
  // IMPORTANT:  You must execute the finish method here to inform the native plugin that you're finished,
  // and the background-task may be completed.  You must do this regardless if your operations are successful or not.
  // IF YOU DON'T, ios will CRASH YOUR APP for spending too much time in the background.
  this.backgroundGeolocation.finish(); // FOR IOS ONLY
  });
  
  });
  
  // start recording location
  this.backgroundGeolocation.start();
  
  // If you wish to turn OFF background-tracking, call the #stop method.
  this.backgroundGeolocation.stop();
  }
  async checkLocation() {
   
    const p = this.diagnostic.isLocationAvailable();
    p.then( (available) => {
        // alert("Location is " + (available ? "available" : "not available"));
        const c = this.diagnostic.requestLocationAuthorization();
        c.then(function (status) {
            console.log('status', status);
        });
    }).catch(function (error) {
        alert('The following error occurred: ' + error);
    });


    const z = this.diagnostic.isLocationEnabled();
    await z.then( (enabled) => {
        console.log('Location setting is ' + (enabled ? 'enabled' : 'disabled'));
        if (!enabled) {

            this.activateGeo();

        } else {
            
        }
    }, function (error) {
        console.error('The following error occurred: ' + error);
    });
}
async activateGeo() {
  const alert = await this.alertController.create({
      header: 'Ubicación requerida',
      message: '<strong>Teem</strong> ' + 'no tiene permisos',
      buttons: [
          {
              text: 'Go to settings',
              handler: () => {
                  console.log('Confirm Okay');
                  this.diagnostic.switchToLocationSettings();
                  // this.checkLocation();
              }
          }
      ]
  });

  await alert.present();
}

async getLocation() {
  const location = await this.backgroundGeolocation.getCurrentLocation();
  console.log('[location]', location);
  // await this.onLocation(location);
  await this.dataService.updateUserData(location);
  return;
}
}
