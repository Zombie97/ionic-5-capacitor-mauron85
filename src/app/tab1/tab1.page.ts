import { Component } from '@angular/core';
import { DataService } from '../services/data/data.service';
import { BackgroundTrackService } from '../services/geolocation/background-track.service';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {
    /**
     * current user's info
     * @typeObject
     */
    userInfo;
    /**
     * subscribe to user info observable
     * @typeObservable
     */
    userInfoSubscribe;
  constructor(private dataService: DataService, private backgroundTrack: BackgroundTrackService) {}
  async ngOnInit() {
    // get user info
    this.userInfoSubscribe = this.dataService.currentUserInfo.subscribe(userInfo => this.userInfo = userInfo);
    // wait for the platform to be ready
  
    await this.waitForUserInfo();
}
async waitForUserInfo() {

  if (this.userInfo.latitude) {
    console.log('USER_INFO', this.userInfo)
  } else {
      setTimeout(async  () => {
          await this.backgroundTrack.getLocation();
          await this.waitForUserInfo();
      }, 1000);
  }
}
}
